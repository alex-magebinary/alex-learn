var example='<div class="row"> <div class="col-xs-12 col-sm-4"> <div class="man"> <div class="head"></div> <div class="body-arms-legs"> <div class="body"> <div class="joint left bottom"> <div class="leg top"> <div class="joint bottom"> <div class="leg top"></div> </div> </div> </div> <div class="joint right bottom"> <div class="leg top"> <div class="joint bottom"> <div class="leg top"></div> </div> </div> </div> </div> <div class="shoulder"> <div class="joint left"> <div class="arm top"> <div class="joint bottom"> <div class="arm top"></div> </div> </div> </div> <div class="joint right"> <div class="arm top"> <div class="joint bottom"> <div class="arm top"></div> </div> </div> </div> </div> </div> </div> </div> <div class="col-xs-12 col-sm-4"> <div class="man"> <div class="head"></div> <div class="body-arms-legs"> <div class="body"> <div class="joint left bottom"> <div class="leg top"> <div class="joint bottom"> <div class="leg top"></div> </div> </div> </div> <div class="joint right bottom"> <div class="leg top"> <div class="joint bottom"> <div class="leg top"></div> </div> </div> </div> </div> <div class="shoulder"> <div class="joint left"> <div class="arm top"> <div class="joint bottom"> <div class="arm top"></div> </div> </div> </div> <div class="joint right"> <div class="arm top"> <div class="joint bottom"> <div class="arm top"></div> </div> </div> </div> </div> </div> </div> </div> <div class="col-xs-12 col-sm-4"> <div class="man"> <div class="head"></div> <div class="body-arms-legs"> <div class="body"> <div class="joint left bottom"> <div class="leg top"> <div class="joint bottom"> <div class="leg top"></div> </div> </div> </div> <div class="joint right bottom"> <div class="leg top"> <div class="joint bottom"> <div class="leg top"></div> </div> </div> </div> </div> <div class="shoulder"> <div class="joint left"> <div class="arm top"> <div class="joint bottom"> <div class="arm top"></div> </div> </div> </div> <div class="joint right"> <div class="arm top"> <div class="joint bottom"> <div class="arm top"></div> </div> </div> </div> </div> </div> </div> </div> </div>';
function create_man () {
	$(".screen").append(example);
}

function rotate_body () {
	$( ".body" ).addClass( "rotate-body" );
	$( ".shoulder" ).addClass( "rotate-shoulder" );
}

function rotate_back () {
	$( ".body" ).removeClass( "rotate-body" );
	$( ".shoulder" ).removeClass( "rotate-shoulder" );
}

function walk () {
	$( ".body>.joint.left" ).toggleClass( "joint-walk-left-up" );
	$( ".body>.joint.left>.leg>.joint" ).toggleClass( "joint-walk-left-down" );
	$( ".body>.joint.right" ).toggleClass( "joint-walk-right-up" );
	$( ".body>.joint.right>.leg>.joint" ).toggleClass( "joint-walk-right-down" );
	$( ".shoulder>.joint.left" ).toggleClass( "joint-walk-right-up" );
	$( ".shoulder>.joint.right" ).toggleClass( "joint-walk-left-up" );
	$( ".shoulder>.joint>.arm>.joint" ).toggleClass( "joint-walk-arm" );
}


$(document).ready(function(){
	$("#test1").click(function() {
		create_man ();
	});
	$("#test2").click(function() {
		rotate_body ();
	});
	$("#test3").click(function() {
		rotate_back ();
	});
	$("#test4").click(function() {
		walk ();
	});
});